Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1985, 1986, 1988-2022, Free Software Foundation, Inc.
License: GPL-3+

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2017, 2020-2022, Free
License: FSFAP

Files: NEWS
Copyright: no-info-found
License: GPL-3

Files: aclocal.m4
Copyright: 1992-2022, Free Software Foundation, Inc.
License: FSFULLR

Files: basicdefs.h
Copyright: 1998-2022, Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/ar-lib
 build-aux/compile
 build-aux/depcomp
 build-aux/mdate-sh
 build-aux/missing
 build-aux/test-driver
Copyright: 1995-2022, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-aux/config.rpath
Copyright: 1992-2022, Free Software Foundation, Inc.
License: FSFULLR

Files: build-aux/gnupload
Copyright: 1998-2022, Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/help2man
Copyright: 1988, 1997-2022, Free Software
License: GPL-3+

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: X11

Files: configure
Copyright: 1992-1996, 1998-2017, 2020-2022, Free Software Foundation
License: FSFUL

Files: doc/sed.info
 doc/sed.texi
Copyright: 1998-2022, Free Software Foundation, Inc.
License: GFDL-1.3+

Files: gnulib-tests/_Noreturn.h
 gnulib-tests/arg-nonnull.h
 gnulib-tests/c++defs.h
 gnulib-tests/warn-on-use.h
Copyright: 2009-2022, Free Software Foundation, Inc.
License: LGPL-2+

Files: gnulib-tests/accept.c
 gnulib-tests/arpa_inet.in.h
 gnulib-tests/bind.c
 gnulib-tests/closedir.c
 gnulib-tests/connect.c
 gnulib-tests/dirent-private.h
 gnulib-tests/dirent.in.h
 gnulib-tests/dirfd.c
 gnulib-tests/explicit_bzero.c
 gnulib-tests/fdopen.c
 gnulib-tests/fflush.c
 gnulib-tests/fopen.c
 gnulib-tests/fpurge.c
 gnulib-tests/freading.c
 gnulib-tests/freading.h
 gnulib-tests/fseek.c
 gnulib-tests/fseeko.c
 gnulib-tests/ftell.c
 gnulib-tests/ftello.c
 gnulib-tests/getcwd-lgpl.c
 gnulib-tests/getpagesize.c
 gnulib-tests/gettimeofday.c
 gnulib-tests/ioctl.c
 gnulib-tests/isblank.c
 gnulib-tests/link.c
 gnulib-tests/listen.c
 gnulib-tests/localename-table.c
 gnulib-tests/localename-table.h
 gnulib-tests/localename.c
 gnulib-tests/localename.h
 gnulib-tests/lseek.c
 gnulib-tests/nanosleep.c
 gnulib-tests/netinet_in.in.h
 gnulib-tests/opendir.c
 gnulib-tests/perror.c
 gnulib-tests/pipe.c
 gnulib-tests/pselect.c
 gnulib-tests/pthread-thread.c
 gnulib-tests/pthread.in.h
 gnulib-tests/pthread_sigmask.c
 gnulib-tests/raise.c
 gnulib-tests/read-file.c
 gnulib-tests/read-file.h
 gnulib-tests/readdir.c
 gnulib-tests/sched.in.h
 gnulib-tests/select.c
 gnulib-tests/setenv.c
 gnulib-tests/setsockopt.c
 gnulib-tests/signal.in.h
 gnulib-tests/sigprocmask.c
 gnulib-tests/sleep.c
 gnulib-tests/socket.c
 gnulib-tests/sockets.c
 gnulib-tests/sockets.h
 gnulib-tests/stdio-impl.h
 gnulib-tests/strerror_r.c
 gnulib-tests/sys_ioctl.in.h
 gnulib-tests/sys_select.in.h
 gnulib-tests/sys_socket.c
 gnulib-tests/sys_socket.in.h
 gnulib-tests/sys_time.in.h
 gnulib-tests/sys_uio.in.h
 gnulib-tests/thread-optim.h
 gnulib-tests/unsetenv.c
 gnulib-tests/w32sock.h
 gnulib-tests/windows-thread.c
 gnulib-tests/windows-thread.h
 gnulib-tests/windows-tls.c
 gnulib-tests/windows-tls.h
Copyright: 1985, 1989-2022, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: gnulib-tests/glthread/*
Copyright: 1985, 1989-2022, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: gnulib-tests/inet_pton.c
Copyright: 2006, 2008-2022, Free Software Foundation, Inc.
License: ISC or LGPL-2.1+

Files: gnulib-tests/putenv.c
Copyright: 1991, 1993, 1994, 1996-2000, 2003-2022, Free Software
License: LGPL-3+

Files: gnulib-tests/setlocale.c
 gnulib-tests/symlink.c
Copyright: 1988-2022, Free Software Foundation, Inc.
License: LGPL-3+

Files: gnulib-tests/strdup.c
Copyright: 1988, 1990-1992, 1994-2022, Free Software
License: LGPL-2.1+

Files: gnulib-tests/test-dynarray.c
Copyright: 1998-2022, Free Software Foundation, Inc.
License: GPL-2+

Files: lib/*
Copyright: 1985, 1989-2022, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/_Noreturn.h
 lib/arg-nonnull.h
 lib/c++defs.h
 lib/warn-on-use.h
Copyright: 2009-2022, Free Software Foundation, Inc.
License: LGPL-2+

Files: lib/acl-errno-valid.c
 lib/acl-internal.c
 lib/acl-internal.h
 lib/acl.h
 lib/acl_entries.c
 lib/close-stream.c
 lib/close-stream.h
 lib/copy-acl.c
 lib/dfa.h
 lib/get-permissions.c
 lib/gnulib.mk
 lib/local.mk
 lib/localeinfo.c
 lib/localeinfo.h
 lib/progname.c
 lib/progname.h
 lib/qcopy-acl.c
 lib/qset-acl.c
 lib/quote.h
 lib/quotearg.c
 lib/set-acl.c
 lib/set-permissions.c
 lib/stat-macros.h
 lib/unlocked-io.h
 lib/xalloc.h
 lib/xmalloc.c
Copyright: 1985, 1986, 1988-2022, Free Software Foundation, Inc.
License: GPL-3+

Files: lib/alloca.c
Copyright: no-info-found
License: public-domain

Files: lib/alloca.in.h
 lib/basename-lgpl.h
 lib/dirname.h
 lib/error.h
 lib/stripslash.c
Copyright: 1990, 1995-1999, 2001-2022, Free Software Foundation
License: LGPL-2.1+

Files: lib/basename-lgpl.c
 lib/c-strcase.h
 lib/dirname-lgpl.c
 lib/getdelim.c
 lib/gettext.h
 lib/hard-locale.c
 lib/minmax.h
 lib/mkostemp.c
 lib/pathmax.h
 lib/realloc.c
 lib/rmdir.c
Copyright: 1988, 1990-1992, 1994-2022, Free Software
License: LGPL-2.1+

Files: lib/cdefs.h
Copyright: The GNU Toolchain Authors.
 1992-2022, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/closeout.c
 lib/fpending.c
 lib/fpending.h
 lib/quotearg.h
Copyright: 1998-2022, Free Software Foundation
License: GPL-3+

Files: lib/closeout.h
 lib/dfa.c
 lib/xalloc-die.c
Copyright: 1988, 1997-2022, Free Software
License: GPL-3+

Files: lib/fwriting.c
 lib/fwriting.h
 lib/mbrlen.c
 lib/obstack.c
 lib/obstack.h
 lib/version-etc-fsf.c
 lib/version-etc.c
 lib/version-etc.h
 lib/wctob.c
Copyright: 1988-2022, Free Software Foundation, Inc.
License: LGPL-3+

Files: lib/memrchr.c
Copyright: 1991, 1993, 1994, 1996-2000, 2003-2022, Free Software
License: LGPL-3+

Files: m4/*
Copyright: 1992-2022, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/alloca.m4
 m4/getpagesize.m4
 m4/malloca.m4
 m4/mbrtowc.m4
 m4/mempcpy.m4
 m4/memrchr.m4
 m4/pathmax.m4
 m4/perl.m4
 m4/quote.m4
Copyright: 1998-2022, Free Software Foundation
License: FSFULLR

Files: m4/codeset.m4
 m4/stat-time.m4
Copyright: 1998-2003, 2005-2022, Free Software
License: FSFULLR

Files: m4/gettext.m4
 m4/intl-thread-locale.m4
 m4/intlmacosx.m4
 m4/po.m4
 m4/progtest.m4
Copyright: 1995-2022, Free Software Foundation, Inc.
License: FSFULLR or GPL or LGPL

Files: m4/gnulib-comp.m4
Copyright: 1985, 1986, 1988-2022, Free Software Foundation, Inc.
License: GPL-3+

Files: m4/lcmessage.m4
Copyright: 1995-2002, 2004, 2005, 2008-2014, 2016, 2019-2022, Free
License: FSFULLR or GPL or LGPL

Files: m4/nls.m4
Copyright: 1995-2003, 2005, 2006, 2008-2014, Free Software Foundation
License: FSFULLR or GPL or LGPL

Files: testsuite/bsd.sh
Copyright: 1992, Diomidis Spinellis.
 1992, 1993, The Regents of the University of California.
License: BSD-4-Clause-UC

Files: ChangeLog testsuite/mac-mf.good testsuite/uniq.good
Copyright: 1989-2020 Free Software Foundation, Inc.
License: GPL-3+

Files: doc/fdl.texi doc/sed.1
Copyright: 1998-2020 Free Software Foundation, Inc.
License: GFDL-NIV-1.3+
 Permission is granted to copy, distribute and/or modify this
 document under the terms of the GNU Free Documentation License,
 Version 1.3 or any later version published by the Free Software
 Foundation; with no Invariant Sections, no Front-Cover Texts, and
 no Back-Cover Texts.  A copy of the license is included in the
 section entitled "GNU Free Documentation License".
 .
 On Debian GNU/Linux systems you can find a copy of the GFDL in
 /usr/share/common-licenses/GFDL-1.3
